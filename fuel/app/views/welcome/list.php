

<?php
//var_dump($target); 
//echo  $_SERVER['REQUEST_TIME'];
//echo"<br>";

//***************************************
// 日時の差を計算
//***************************************
function time_diff($time_from, $time_to) 
{
    // 日時差を秒数で取得
    $dif = $time_to - $time_from;
    // 時間単位の差
    $dif_time = date("H時間i分s秒", $dif);
    $dif_time;
    // 日付単位の差
    $dif_days = (strtotime(date("Y-m-d", $dif)) - strtotime("1970-01-01")) / 86400;
    return "{$dif_days}日 {$dif_time}";
}


$today = strtotime("now");
echo date("現在日時:Y/m/d/H:i:s",$today);
?>
<form action="<?php echo $action; ?>" method="post">
                <?php if($error !='') : ?>
                <span class="red"><?= $error ?></span><br>
                <?php endif; ?>
                <?php if($success !='') : ?>
                <span class="blue"><?= $success ?></span><br>
                <?php endif; ?>
                目標日時:<input type="text" name="目標日時" value="<?= $目標日時 ?>"><br>
                目標:<input type="text" name="目標" value="<?= $目標 ?>"><br>        
               <?php if($is_update) : ?>
                <input type="hidden" name="id" value="<?= $id ?>">
                <input type="submit" name="save" value="更新">
                <?php else : ?> 
                <input type="submit" name="insert" value="登録">
              <?php endif; ?>
 </form>
 <table border="1">
    <tr>
      <th>ID</th>
      <th>目標日時</th>
      <th>残り時間</th>
      <th>目標</th>
      <th>完了</th>
    </tr>
    <?php foreach ($target as $t) : ?>

    <tr>
      <td><?php echo $t['id']; ?></td>
      <td><?php echo $t['目標日時']; ?></td>
      <td>
        <?php
              $to = strtotime($t['目標日時']);
          
          echo time_diff($today, $to);
        ?>
      </td>
      <td><?php echo $t['目標']; ?></td>
      <td><?php echo $t['完了']; ?>
        <form action="<?php echo Uri::create('welcome/done'); ?>" method="post">  
          <input type="hidden" name="done_id" value="<?= $t['id']; ?>">
          <input type="submit" name="done" value="完了">
        </form>
      </td>
      <td>
        <form action="<?php echo Uri::create('target'); ?>" method="post">  
          <input type="hidden" name="update_id" value="<?= $t['id']; ?>">
          <input type="submit" name="update" value="編集">
        </form>   
        <form action="<?php echo Uri::create('target/delete'); ?>" method="post">  
          <input type="hidden" name="id" value="<?= $t['id']; ?>">
          <input type="submit" name="delete" value="削除" 
                 onclick="if(confirm('削除してよろしいですか？'))
                 {return true;} else {return false;}">
                     </form>   
                    </td>
                </tr>   
    <?php endforeach; ?>
   </table>
    
    