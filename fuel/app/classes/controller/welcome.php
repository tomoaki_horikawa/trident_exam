<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
    
    public function action_list() {
        $data = array();

        $data['error'] = '';
        $data['success'] = Session::get_flash('success','');
        $data['action'] = Uri::create('welcome/insert');
        $data['is_update'] = false;
        $data['目標日時'] = '';
        $data['目標'] = '';
        $data['target'] = Model_Target::find('all');
        return Response::forge(View::forge('welcome/list',$data));
        
    }
    
    public function post_done() {
       $id = Fuel\Core\Input::post('done_id',0);
       $t = Model_Target::find($id);
       $t->完了= date('Y/m/d H:i:s');
       $t->save();
       
       return \Fuel\Core\Response::redirect('welcome/list');
    }
    
	/**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
            $data['目標日時'] = '';
            $data['目標'] = '';        
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
